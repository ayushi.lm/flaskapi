from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_login import login_user, current_user, logout_user, login_required, LoginManager, UserMixin

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///ashdb.db'
app.config['SECRET_KEY']="anything"
db = SQLAlchemy(app)
ma = Marshmallow(app)
login_manager=LoginManager(app)
login_manager.login_view='login'
login_manager.login_message_category='info'


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(20), nullable=False)
    email = db.Column(db.String(20), unique=True, nullable=False)
    phone = db.Column(db.String(10), unique=True)
    address = db.Column(db.String(50))
    contacts=db.relationship('Contact', backref='owner', lazy=True)

    def __init__(self, username, password, email, phone, address):
        self.username = username
        self.password = password
        self.email = email
        self.phone = phone
        self.address = address


class UserSchema(ma.Schema):
    class Meta:
        fields = ('username', 'phone', 'email', 'address')


user_schema = UserSchema()
users_schema = UserSchema(many=True)

class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    email = db.Column(db.String(20), unique=True, nullable=False)
    phone = db.Column(db.String(10), unique=True)
    address = db.Column(db.String(50))
    country = db.Column(db.String(50))
    user_id=db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)


    def __init__(self, name, email, phone, address, country, owner):
        self.name = name
        self.email = email
        self.phone = phone
        self.address = address
        self.country = country
        self.owner=owner


class ContactSchema(ma.Schema):
    class Meta:
        fields = ('name', 'phone', 'email', 'address', 'country')


contact_schema = ContactSchema()
contacts_schema = ContactSchema(many=True)

# creating database
with app.app_context():
    db.create_all()

# create a new user
@app.route('/signup', methods=["POST"])
def signup():
    username = request.json['username']
    password = request.json['password']
    phone = request.json['phone']
    email = request.json['email']
    address = request.json['address']
    new_user = User(username=username, password=password, phone=phone, email=email, address=address)
    db.session.add(new_user)
    db.session.commit()
    user = User.query.get(new_user.id)
    return user_schema.jsonify(user)

@app.route("/login", methods=['GET', 'POST'])
def login():
    password = request.json['password']
    email = request.json['email']
    user=User.query.filter_by(email=email).first()
    if user and user.password==password:
        login_user(user)
        return user_schema.jsonify(user)
    else:
        return "Login Failed!"

@app.route("/logout")
@login_required
def logout():
    logout_user()
    return "Logged Out successfully"


# list all users
@app.route("/getusers", methods=["GET"])
def get_users():
    all_user = User.query.all()
    result = users_schema.dump(all_user)
    return jsonify(result)


# get single user
@app.route("/user/<id>", methods=["GET"])
def get_user(id):
    user = User.query.get(id)
    return user_schema.jsonify(user)


# update user
@app.route("/user/<id>", methods=["PUT"])
@login_required
def user_update(id):
    user = User.query.get(id)
    username = request.json['username']
    password = request.json['password']
    phone = request.json['phone']
    email = request.json['email']
    address = request.json['address']
    user.username = username
    user.password = password
    user.phone = phone
    user.email = email
    user.address = address
    db.session.commit()
    return user_schema.jsonify(user)


# delete user
@app.route("/user/<id>", methods=["DELETE"])
def user_delete(id):
    user = User.query.get(id)
    db.session.delete(user)
    db.session.commit()
    return "User was successfully deleted"


# create a new contact
@app.route('/contact', methods=["POST"])
@login_required
def add_contact():
    name = request.json['name']
    phone = request.json['phone']
    email = request.json['email']
    address = request.json['address']
    country = request.json['country']
    new_contact = Contact(name=name, phone=phone, email=email, address=address, country=country, owner=current_user)
    db.session.add(new_contact)
    db.session.commit()
    user = Contact.query.get(new_contact.id)
    return contact_schema.jsonify(user)


@app.route("/getcontact", methods=["GET"])
@login_required
def get_contacts():
    all_contact = Contact.query.filter_by(owner=current_user)
    result = contacts_schema.dump(all_contact)
    return jsonify(result)


# update contact
@app.route("/contact/<id>", methods=["PUT"])
@login_required
def contact_update(id):
    contact = Contact.query.get(id)
    if contact.owner!=current_user:
        return "Invalid Request"
    else:
        name = request.json['name']
        phone = request.json['phone']
        email = request.json['email']
        address = request.json['address']
        country = request.json['country']
        contact.name = name
        contact.phone = phone
        contact.email = email
        contact.address = address
        contact.country = country
        db.session.commit()
        return contact_schema.jsonify(contact)

# delete contact
@app.route("/contact/<id>", methods=["DELETE"])
@login_required
def contact_delete(id):
    contact = Contact.query.get(id)
    if contact.owner!=current_user:
        return "Invalid Request"
    else:
        db.session.delete(contact)
        db.session.commit()
        return "Contact was successfully deleted"


if __name__ == '__main__':
    app.run(debug=True)